import React from 'react';
import './App.scss';
import Gallery from '../Gallery';
import Expand from '../Extentions/Expand/expand'
import Menu from '../Extentions/Menu/Menu'
import Preferences from '../Extentions/Preferences/Preference'
import AlbumGalley from '../Extentions/AlbumGallery/AlbumGallery'


class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'israel',
      showFavorite:0,
      sizeChanged:0,
      album:0
    };
  }


  handleGallery=()=>
{
  if(this.state.album == 1)
     return (<AlbumGalley/>)
  return <Gallery tag={this.state.tag} showFavorite = {this.state.showFavorite}  sizeChanged = {this.state.sizeChanged}/>;
}

  render() {
    return (
      <div className="app-root">
        <div className="app-header">
        <Menu  onFavorite = {()=>{this.setState({showFavorite:1})}} onReturn= {()=>{this.setState({album:0,favorite:0})}} onAlbum = {()=>{this.setState({album:1})}}></Menu>
        <Preferences  onchange = {()=>{this.setState({sizeChanged:1})}}></Preferences>
          <h2>Flickr Gallery</h2>
          <input className="app-input" 
          onChange={event => this.setState({tag: event.target.value,showFavorite:0,album:0})} 
          value={this.state.tag}    placeholder="Search your best photos"/>
        </div>
        {this.handleGallery()}
        <Expand id = "expand_component" degree = "0"></Expand>
   
      </div>
    );
  }
}

export default App;

import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';
import Gallery from '../Gallery';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number,
    
  };

  constructor(props) {
    super(props);
    this.calcImageSize = this.calcImageSize.bind(this);
    this.state = {
      size:this.getSizeImage(),
      degree: 90,
      scale: 1,
      iconFavorite: this.checkStatusOfFavorite(),
      iconFavoriteTitle: 'add to favorite',
    };
  }

  calcImageSize() {
    const {galleryWidth} = this.props;
    const targetSize = this.state.size;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = (galleryWidth / imagesPerRow);
    this.setState({
      size
    });
  }

  componentDidMount() {
    this.calcImageSize();
  }
  

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  initFavorite=()=>
  {
    if(localStorage.getItem('favorite') == null)
          localStorage.setItem('favorite',JSON.stringify([]));
  }

  
  checkStatusOfFavorite=()=>{
    var tmp =  JSON.parse(localStorage.getItem('favorite'));
    var idListFavorite = [];
    tmp.map(dto => idListFavorite.push(dto.id));
    if(idListFavorite.includes(this.props.dto.id) == true)
         return 'heart';
    
    return 'star';
  }

  handleFlip =()=>{ 
    this.setState({scale:  this.state.scale*-1}); // this will change from 1 to -1 and also from -1 to 1 ((-1)*(-1) = 1) 
  }

  handleExpand= (url)=>{
    const expand = document.getElementById('expand_id');
    const image = document.getElementById('expand-image-id');
    const iconBox = document.getElementById('expand-icons-box_id');
    var linkimage = document.getElementById('link_id');
   
    /*display and update the image*/  
    expand.style.display = 'block';
    image.src = url;
    linkimage.href = url;
   /*responsivity*/
   iconBox.style.left = `${document.body.clientWidth/2.5}px`;
   }

   getSizeImage=()=>{
     if(localStorage.getItem('sett-size-option1') == 'large')
         return 260;
     
        if(localStorage.getItem('sett-size-option1') == 'small')
        return 140;
        if(localStorage.getItem('sett-size-option1') == 'medium')
        return 200;

   }
   getSizeFontIcon=()=>
   {
    if(localStorage.getItem('sett-size-option1') == 'large')
    return 28;
   if(localStorage.getItem('sett-size-option1') == 'small')
   return 14;
   if(localStorage.getItem('sett-size-option1') == 'medium')
   return 18;
   }

   getHeightIcon=()=>{
    if(localStorage.getItem('sett-size-option1') == 'large')
    return 42;

   if(localStorage.getItem('sett-size-option1') == 'small')
   return 27;
   if(localStorage.getItem('sett-size-option1') == 'medium')
   return 29;
   }
   
   handleFavorite =()=>{
     this.initFavorite();
    if(this.state.iconFavorite == 'star'){
      var tmp =  JSON.parse(localStorage.getItem('favorite'));
      tmp.push(this.props.dto);
      localStorage.setItem('favorite', JSON.stringify(tmp));
      this.setState({iconFavorite : 'heart'});
      this.setState({iconFavoriteTitle : 'remove from favorite'}); 
        return;
     }
     var tmp =  JSON.parse(localStorage.getItem('favorite'));
     var indx = tmp.indexOf(this.props.dto);
     tmp.splice(indx, 1);
     localStorage.setItem('favorite', JSON.stringify(tmp));
     this.setState({iconFavorite : 'star'});
     this.setState({iconFavoriteTitle : 'add to favorite'});
  }

  
  render() {
    return (
      <div
        className="image-root"
        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: `${this.getSizeImage()}` + 'px',
          height: `${this.getSizeImage()}` + 'px',
          webkitTransform:`scaleX(${this.state.scale})`,
          transform :`scaleX(${this.state.scale})`
        }}  draggable="true"
        onDragStart = {()=>{this.props.ondrag(this.props.dto)
          }}
        onDragOver = {event =>{event.preventDefault();}} 
        onDrop ={ event=> {event.preventDefault();this.props.ondrop(this.props.dto)}}
        >
      <div>
        <div style={{
      
          webkitTransform:`scaleX(${this.state.scale})`,
          transform :`scaleX(${this.state.scale})`,
          
          
        }}
        >
          <FontAwesome id = "img-icn"className="image-icon" name="arrows-alt-h" title="flip" 
            style = {{fontSize:`${this.getSizeFontIcon()}px`, height:`${this.getHeightIcon()}px`}}
             onClick = {()=>{this.handleFlip()}} />
          <FontAwesome id = "img-icn" className="image-icon" name="clone" title="clone"
          style = {{fontSize:`${this.getSizeFontIcon()}px`, height:`${this.getHeightIcon()}px`}}
            onClick = {()=> {this.props.onClone(this.props.dto)}}/>
          <FontAwesome id = "img-icn"className="image-icon" name="expand" title="expand"
          style = {{fontSize:`${this.getSizeFontIcon()}px`, height:`${this.getHeightIcon()}px`}}
           onClick = {()=>{this.handleExpand(this.urlFromDto(this.props.dto))}}/></div>

          <div
          style={{
      
            webkitTransform:`scaleX(${this.state.scale})`,
            transform :`scaleX(${this.state.scale})`
          }} >
          <FontAwesome id = "img-icn" className="image-icon" name= {this.state.iconFavorite} title={this.state.iconFavoriteTitle}
          style = {{fontSize:`${this.getSizeFontIcon()}px`, height:`${this.getHeightIcon()}px`}}
             onClick = {()=>{this.handleFavorite()}} />
            <span id = "rotate_id" className = "image-icon" style = {{fontSize:`${this.getSizeFontIcon()}px`, height:`${this.getHeightIcon()}px`}}
            onClick = {()=>{this.props.AddToALb(this.props.dto)}}>
                        <i className= "fas fa-images" title="addToAlbum" 
                        ></i>
                    </span>
         
         </div>
      </div>
      
      </div>
    );
  }
}

export default Image;

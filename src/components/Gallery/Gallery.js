import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';
import cloneDeep from 'lodash/cloneDeep';



class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string
  };

  constructor(props) {
    super(props);
    this.state = {
      images: [],
      galleryWidth: this.getGalleryWidth(),
      page: 1,
      showFav:this.props.showFavoriteG,
      draggedElem:'',
      sizeChanged:0,
      urlImageToAddAlb:''
    };
  }

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }
  getImages(tag) {

    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=100&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
         
          this.setState({images: res.photos.photo});
        }
      });
  }

  getFavorite =()=>{
      const json = JSON.parse(localStorage.getItem('favorite'));
      this.setState({images:json});
    
  }

  componentDidMount() {
   
    this.getImages(this.props.tag);
   this.setState({
      galleryWidth: document.body.clientWidth,
      showFav:this.props.showFavorite
    });
  }

  componentWillReceiveProps(props) {
    this.state.showFav = props.showFavorite;
    this.state.page = 1;
    if(this.state.showFav == 0)
      this.getImages(props.tag);

    if(this.state.showFav == 1)
    {
      this.getFavorite();
      window.removeEventListener('scroll', this.handleScroll);
    }
      
  }


  getNewImages=(tag)=>
  {
    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=100&format=json&safe_search=1&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
    const initial = this.state.images;
    var arr =  cloneDeep(res.photos.photo);
    for(var i = 0 ; i <arr.length; i++)
          initial.push (arr[i]);
    this.setState({images: initial});}
  });

  }
  handleClone = (dto)=> {
    var indexOfClonedElement = this.state.images.lastIndexOf(dto);
    this.state.images.splice(indexOfClonedElement+1,0,dto);
    this.setState({images :this.state.images}); 
 }

 handleScroll=()=>{
  if(document.body.clientHeight- pageYOffset < (3000))
   {
     this.state.page +=1;
     window.removeEventListener('scroll', this.handleScroll);
     this.getNewImages(this.props.tag);
 }
}

handleDrag = (dto)=>{
   this.state.draggedElem = dto; 
}
handleDrop =(dto)=>{
 
  var indexDraggedElem = this.state.images.indexOf( this.state.draggedElem);
  var indexDropped =  this.state.images.indexOf( dto);
  this.state.images[indexDropped] =  this.state.draggedElem;
  this.state.images[indexDraggedElem] = dto;
  this.setState({images:this.state.images});
}
addScrollListener=()=>{
  if(this.props.showFavorite == 0){ 
    window.addEventListener('scroll', this.handleScroll);}
}

handleToWichAlbum = ()=>
{
  var lclst = JSON.parse(localStorage.getItem('albums'));
  if(localStorage.length == 0){
  return(
    <div id = 'to-wich-alb'  className = "wich-album">
      <p>no album !</p>
      <button  id = 'aab-cancel'  type="button" className="w3-btn w3-red"
      onClick = {()=>{this.handleCloseAddAlb()}}>ok</button></div>);
  }
  
  return(
    <div id = 'to-wich-alb' className = "wich-album" >
      <p>To wich album to add ?</p>
        { lclst.map((albumName,index) => {return <button  id = 'aab-cancel' key ={index }type="button" className="w3-btn w3-red"
        onClick = {()=>{this.handleToWichAlbAdd(albumName)} }>  {albumName}</button>;})}
        <br/>
        <button  id = 'aab-cancel' type="button" className="w3-btn w3-red"
            onClick = {()=>{this.handleCloseAddAlb()} }    >Cancel</button>
     </div>);

  
  }
  handleAddToALB=(dto)=>{
    const toWichAlb = document.getElementById('to-wich-alb');
    toWichAlb.style.display = 'block';
    this.state.urlImageToAddAlb =dto;
  }

  handleCloseAddAlb=()=>{
    const toWichAlb = document.getElementById('to-wich-alb');
    toWichAlb.style.display = 'none';
  }

  handleToWichAlbAdd=(albumName)=>{
    var lclstr =JSON.parse( localStorage.getItem(`${albumName}`));
    lclstr.push(this.state.urlImageToAddAlb);
    localStorage.setItem(`${albumName}`, JSON.stringify(lclstr));
    const toWichAlb = document.getElementById('to-wich-alb');
    toWichAlb.style.display = 'none';
  }

 
  render() {
     
    return (
      
      <div className="gallery-root">
        {this.addScrollListener()}
        {this.state.images.map((dto,index) => {
          return <Image key={`image-${index}` + dto.id} dto={dto} galleryWidth={this.state.galleryWidth}
          onClone = {this.handleClone} ondrag = {()=>this.handleDrag(dto)} 
          ondrop ={()=>this.handleDrop(dto)}   AddToALb = {()=>this.handleAddToALB(dto)} degree ="0"
         />
         
        })}

       
              {this.handleToWichAlbum()}
                      
        </div>
         
      
    );
  }
}

export default Gallery;

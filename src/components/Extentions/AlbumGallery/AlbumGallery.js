import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import './AlbumGallery.scss';
import Album from '../Album/Album';
import './w3.css';



class AlbumGallery extends React.Component {
  static propTypes = {
  };

  constructor(props) {
    super(props);
    this.state = {
      albums: [],
      galleryWidth: this.getGalleryWidth(),
      nameToNewAlbum:'',
      descriptionToNewAlbum:''
    };
  }


  initLocalSTR=()=>
{
 // if(localStorage.getItem('albums') == null)
  //     localStorage.setItem('albums', JSON.stringify([]));
}

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }
  getAlbums(tag) {
   
    this.setState({albums: JSON.parse(localStorage.getItem('albums'))});
  }

  componentDidMount() {
    this.initLocalSTR();
    this.getAlbums(this.props.tag);
    this.setState({
      galleryWidth: document.body.clientWidth
    });
  }

  componentWillReceiveProps(props) {
    this.getAlbums(props.tag);
  }

  OpenAddAlbumBox =()=>{
    var addAlbumBox = document.getElementById('add-album-box');
    var close = document.getElementById('aab-close');
    var cancel =  document.getElementById('aab-cancel');
    addAlbumBox.style.display = 'block';
  
  close.onclick = function(){
        addAlbumBox.style.display = 'none';
  }
  cancel.onclick = function(){
    addAlbumBox.style.display = 'none';
     }

  }

  handleAddAlbum=()=>{
    var addAlbumBox = document.getElementById('add-album-box');
    //check if the items exist
    var lclstr = JSON.parse(localStorage.getItem('albums'));
    if(lclstr.includes(this.state.nameToNewAlbum)==false){
        lclstr.push(this.state.nameToNewAlbum);
       this.state.albums= lclstr;
       this.setState({albums: this.state.albums});
       addAlbumBox.style.display = 'none';
       localStorage.setItem('albums', JSON.stringify(this.state.albums));
       localStorage.setItem(`${this.state.nameToNewAlbum}`, JSON.stringify([]));
      }
      addAlbumBox.style.display = 'none';
    return;

  }

  handleRemoveAlbum=(name)=>{
    var lclstr = JSON.parse(localStorage.getItem('albums'));
    
    var indxToSplice = lclstr.indexOf(name);
    lclstr.splice(indxToSplice,1);
    this.state.albums = lclstr;
    this.setState({albums:this.state.albums});
    localStorage.setItem('albums', JSON.stringify(this.state.albums));
    localStorage.removeItem(`${name}`);
   }

 
render() {
    return (
      <div className="gallery-root">
        {this.state.albums.map((dto,index) => {
          return <Album key = {index} name = {dto} img ="4.jpg" onRemove = {()=>{this.handleRemoveAlbum(name)}} onAdd = {()=>this.OpenAddAlbumBox()}/>;
        })}
        
        <Album  img = "none" onAdd = {()=>this.OpenAddAlbumBox()}/>

          <div id="add-album-box" className="w3-modal">
              <span  id = "aab-close" className="w3-closebtn w3-hover-red w3-container w3-padding-16 w3-display-topright w3-xxlarge"
                  > &times;</span>
                    <div className="w3-modal-content w3-card-8 w3-animate-zoom" style={{maxWidth:'500px'}}>
                        <div className="w3-container">
                            <div className="w3-section"></div>
                              <label><b>Input a name for your album<br/></b></label>
                              <input className="w3-input w3-border w3-margin-bottom" type="text" placeholder="name of your album"  
                                    onChange={event => this.setState({nameToNewAlbum: event.target.value})}/>
                      
                              <label><b>Optional: Add a description</b></label>
                              <input className="w3-input w3-border" type="text" placeholder="Add a description"/>
                              
                              <button id = 'aab-add' className="w3-btn w3-btn-block w3-green w3-section"
                              onClick = {()=>this.handleAddAlbum()}>Add</button>
                              
                            </div>
                           <div className="w3-container w3-border-top w3-padding-16 w3-light-grey">
                                  <button  id = 'aab-cancel' type="button" className="w3-btn w3-red"
                                  >Cancel</button>
                            </div>
                        </div>
                      
                      </div>
                

        
            
      </div>
    );
  }
}

export default AlbumGallery;
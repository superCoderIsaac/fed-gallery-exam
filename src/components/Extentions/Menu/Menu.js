import React from 'react';
import './Menu.scss';

class Menu extends React.Component{
    static propTypes = {
    };
  
    constructor() {
      super();
    }

    handlePreferences = ()=>{
        var fp = document.getElementById('frame-preferences-id');
        fp.style.display = 'block';
    }

    render() {
        return (
    <div className="app-menu">
        <span id = "menu-preferences" className = "app-menu-content" onClick = {()=>{this.props.onReturn()}}><i  className= "fa fa-arrow-left" style = {{fontSize:"14px",color:'white'}}title="return"></i></span>
       <span id = "menu-preferences" className = "app-menu-content" onClick = {()=>{this.handlePreferences()}}>Your Preferences</span>
       <span id = "menu-favorite" className = "app-menu-content" onClick = {()=>{this.props.onFavorite()}}>Your Favorite</span>
       <span id = "menu-albums" className = "app-menu-content"onClick = {()=>{this.props.onAlbum()}}>Your Albums</span>
    </div>
        );
    }

}
export default Menu;
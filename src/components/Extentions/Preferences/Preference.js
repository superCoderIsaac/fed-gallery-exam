import React from 'react';
import './Preferences.scss';
import './w3.css';

class Preferences extends React.Component{
    static propTypes = {
    };
  
    constructor() {
      super();
      this.state = {
        ToChange:0,
        Source : '',
        Target:'',
        dropdownChanged :0

    
      };
    }

    componentDidMount() {
        this.initializeLocalStorage();
      }

initializeLocalStorage=()=>{
    if(localStorage.getItem('sett-size-option1') == null)
   {
        localStorage.setItem('sett-size-option1','medium');
        localStorage.setItem('sett-size-option2','large');
        localStorage.setItem('sett-size-option3','small') ;
        localStorage.setItem('sett-dropdown-option1','enable');
        localStorage.setItem('sett-dropdown-option2','disable');
    }
}

handleCheckedSizeImage=(number)=>{
    this.state.ToChange = number;
    this.state.Source =  localStorage.getItem('sett-size-option1');
    this.state.Target =  localStorage.getItem(`sett-size-option${number}`);
  
}

handleCheckedDropdown=()=>{
   this.setState({dropdownChanged:1});
}

handleCancel=()=>{
  var frame = document.getElementById('frame-preferences-id');
  frame.style.display= 'none';
  this.setState({Source:''});
}

handleApplyChanges=()=>{
    if(this.state.Source != '')
    {
     localStorage.setItem('sett-size-option1',`${this.state.Target}`);
     localStorage.setItem(`sett-size-option${this.state.ToChange}`,`${this.state.Source}`); 
    var frame = document.getElementById('frame-preferences-id');
    }
    frame.style.display= 'none';
    this.setState({Source:''});
    this.props.onchange();
}


    render() {
        return (
        <div id = "frame-preferences-id" className ="frame-preferences">
            <div className = "fp-inner">

              <div id = "fp-images-size-id" className = "fp-content">
                    <p style = {{float: 'left',color: 'black', fontSize:'14px'}}>Set Your Image Size</p><br/>
                    <form  className="w3-container w3-card-4">
                    
                            <p style = {{ fontSize:'16px' }}>
                            <input style = {{float: 'left'}} className="w3-radio" type ="radio" name="gender"  checked/>
                            <label style = {{float: 'left' ,margin :'4px'}} className="w3-validate">
                                  {localStorage.getItem('sett-size-option1')}</label></p><br/>
                            
                            <p style = {{ fontSize:'16px' ,margin:'0px'}}>
                            <input style = {{float: 'left'}} className="w3-radio" type="radio" name="gender" onClick ={()=>{this.handleCheckedSizeImage(2)}} />
                            <label style = {{float: 'left',margin :'4px'}} className="w3-validate">
                                      {localStorage.getItem('sett-size-option2')}  </label></p><br/>
                            
                            <p  style = {{ fontSize:'16px' ,margin:'0px'}}>
                            <input style = {{float: 'left'}} className="w3-radio" type="radio" name="gender" value="" onClick ={()=>{this.handleCheckedSizeImage(3)}}/>
                            <label style = {{float: 'left',margin :'4px'}} className="w3-validate">
                                      {localStorage.getItem('sett-size-option3')}</label></p><br/>
                            
                     </form>
             </div>
    
    
            
            <button  type="button" className="w3-btn w3-red"  style = {{fontSize:'12px',margin:'8px'}}
            onClick = {()=>{this.handleCancel()}}>Cancel</button>
            <button  type="button" className="w3-btn w3-red" style = {{fontSize:'12px',margin:'8px'}}
               onClick = {()=>{this.handleApplyChanges()}}>Apply the changes</button>
            </div>
        </div>
    
        
    
           

        );
    }

}
export default Preferences;

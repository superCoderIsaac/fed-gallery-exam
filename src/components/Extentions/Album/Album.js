import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Album.scss';
import './w3.css';


class Album extends React.Component {
  static propTypes = {
    galleryWidth: PropTypes.number,
    img :PropTypes.string
    
  };

  constructor(props) {
    super(props);
    this.calcAlbumSize = this.calcAlbumSize.bind(this);
    this.state = {
      size: 200,
      name:''
    };
  }

  calcAlbumSize() {
    const {galleryWidth} = this.props;
    const targetSize = 200;
    const imagesPerRow = Math.round(galleryWidth / targetSize);
    const size = (galleryWidth / imagesPerRow);
    this.setState({
      size,
      photoIterator:0
    });
  }

  componentDidMount() {
    this.calcAlbumSize();
    this.state.name = this.props.name;
  }

  urlFromDto() {
    var lclstr =JSON.parse( localStorage.getItem(`${this.state.name}`));
     if(this.props.img != 'none')
      {
       if(lclstr != null && lclstr.length > 0)
       {
           return `https://farm${lclstr[this.state.photoIterator].farm}.staticflickr.com/${lclstr[this.state.photoIterator].server}/${lclstr[this.state.photoIterator].id}_${lclstr[this.state.photoIterator].secret}.jpg`;
       }}
  }

  handleShow=()=>{
    if(this.props.img != 'none')
    {
        var album = document.getElementById(`album-root-id${this.state.name}`);
        var lclstr =JSON.parse( localStorage.getItem(`${this.state.name}`));
        if(lclstr.length > 0){
            var url = `https://farm${lclstr[this.state.photoIterator].farm}.staticflickr.com/${lclstr[this.state.photoIterator].server}/${lclstr[this.state.photoIterator].id}_${lclstr[this.state.photoIterator].secret}.jpg`
            this.state.photoIterator = (this.state.photoIterator + 1)%(lclstr.length);
            album.style.backgroundImage = `url(${url})`;
        }
    }
  }



handleAlbumToDispl = ()=>{
    if(this.props.img != 'none') 
         return(<span className = "album-name">{this.state.name}</span>);
    return (<span  onClick = {()=>{this.props.onAdd()}}>add your album <br/> +</span>)
  }
handleDisplRemoveButton=() =>{
  if(this.props.img != 'none') 
  return (<button onClick = {()=>this.props.onRemove(this.state.name)}>remove</button>);
} 
  
   

  render() {
    return (
      <div
        id = {"album-root-id" + this.state.name}className="album-root"
        style={{
          backgroundImage: `url(${this.urlFromDto()})`,
          width: this.state.size + 'px',
          height: this.state.size + 'px',
          
        }}  onClick = {()=>{this.handleShow()}}
        >
           {this.handleAlbumToDispl() }
           {this.handleDisplRemoveButton() }
              
      </div>
    );
  }
}

export default Album;

import React from 'react';
import PropTypes from 'prop-types';
import './expand.scss'

class Expand extends React.Component {
    static propTypes = {
       degree:PropTypes.number
    };
  
    constructor(props) {
      super(props);
      this.state = {
          degree : this.props.degree
      };
    }


    handleRotate= ()=>{
       
        /* handle the icons*/
        const iconBox = document.getElementById('expand-icons-box_id');
        const close = document.getElementById('close_id');
        var image = document.getElementById('expand-image-id');
        const rotateActivate = document.getElementById('rotate-active-id');
        /*update the degree and transform*/ 
        this.state.degree = (this.state.degree +90)%360;
        image.style.webkitTransform =  `rotate(${this.state.degree}deg)`;
        image.style.msTransform =  `rotate(${this.state.degree}deg)`;
        image.style.transform = `rotate(${this.state.degree}deg)`;
        rotateActivate.style.display = 'block';
        iconBox.style.display = 'none';
      
        /*update the location of the box if we endeed to rotate*/
        if(this.state.degree == 0)
        {
          iconBox.style.display = 'block';
           rotateActivate.style.display = 'none';
        }
        
    };

        handleClose=() =>{
            const expand = document.getElementById('expand_id');
            const iconBox = document.getElementById('expand-icons-box_id');
            var image = document.getElementById('expand-image-id');
            this.setState({degree:0});
            /*reset the image scale*/ 
            image.style.webkitTransform =  `rotate(${0}deg)`;
            image.style.msTransform =  `rotate(${0}deg)`;
           image.style.transform = `rotate(${0}deg)`;
            expand.style.display = 'none';
            iconBox.style.display = 'block';
            
            ;}

    render(){
        return(
             
            <div id = "expand_id" className="expand">
                <span id = "close_id" className="close" onClick = {()=>{this.handleClose()}}>&times;</span>
                <div id ="expand-icons-box_id" className = "expand-icons-box">

                    <a download  id = "link_id" className ="expand-icons" style = {{color:'white'}}
                    title="link" href="">
                       <i className= "fa fa-link " ></i>
                    </a>

                    <span id = "rotate_id" className = "expand-icons" >
                        <i className= "fas fa-redo fa-spin" style = {{color:'white'}}title="clone" 
                        onClick = {()=>this.handleRotate()}></i>
                    </span>

                    

                   
                 </div>
                 <span id = "rotate-active-id" className = "rotate-active">
                        <i className= "fas fa-redo fa-spin"  style ={{color:'white'}} title="rotate"
                        onClick = {()=>this.handleRotate()}></i>
                    </span>

                 <img id="expand-image-id" className="expand-image"  name = "0" />
            </div>
        );
    }
  }

  export default Expand;